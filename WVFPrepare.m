%{
    Copyright (C) 2020  Zeus Martí Díaz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}

function WVFPrepare(CORFamily,wvf_num,WvfScaling,wvfstepsize)
%% Function Name: WVFPrepare
% WVFPrepare(CORFamily,wvf_num,WvfScaling,wvfstepsize)
%
% Assumptions: None
%
% Inputs:
%  CORFamily: Corrector family 'HCM' or 'VCM'
%  wvf_num: number waveform to be selected 
%  WvfScaling: waveform amplitude [in Amps]
%  wvfstepsize: temporal waveform step size [us] >80us

%
% Outputs:
%   None

%
% Date: Jan 30, 2020
% ________________________________________

ao=getao;
dev_names=ao.(CORFamily).DeviceName;
dev_list=ao.(CORFamily).DeviceList;
if strcmp(CORFamily,'HCM')||strcmp(CORFamily,'VCM')
    ncorr=6;
elseif strcmp(CORFamily,'QS')
    ncorr=2;
else
    error('Wrong family name gives...');
end

fprintf('Preparing device parameters for continous wave...\n');
list_all_corr=1:size(dev_list,1);
pausetime=0.5;

tic;
fprintf('%s Corrector num ',CORFamily);
for jj=1:ncorr
    fprintf('%d/%d StepSize sent... ',jj,ncorr);
    list=dev_list(dev_list(:,2)==jj,1)';
    command=sprintf('acbdChangeSRCorrStepSize.py %d',wvfstepsize);
    for ii=list
        kk=list_all_corr((dev_list(:,1)==ii)&(dev_list(:,2)==jj));
        command=[command sprintf(' %s',dev_names{kk})];
    end
    system(command);
    toc;
    if jj<ncorr
        for ii=1:55
            fprintf('\b');
        end
    end
end

tic;
fprintf('%s Corrector num ',CORFamily);
for jj=1:ncorr
    fprintf('%d/%d WVFAutoTrigger sent... ',jj,ncorr);
    list=dev_list(dev_list(:,2)==jj,1)';
    for ii=list
        kk=list_all_corr((dev_list(:,1)==ii)&(dev_list(:,2)==jj));
        tango_write_attribute(dev_names{kk},'WVFAutoTrigger',uint8(1));
    end
    pause(pausetime);
    toc;
    if jj<ncorr
        for ii=1:61
            fprintf('\b');
        end
    end
end

tic;
fprintf('%s Corrector num ',CORFamily);
for jj=1:ncorr
    fprintf('%d/%d WVFSelected sent... ',jj,ncorr);
    list=dev_list(dev_list(:,2)==jj,1)';
    for ii=list
        kk=list_all_corr((dev_list(:,1)==ii)&(dev_list(:,2)==jj));
        tango_write_attribute(dev_names{kk},'WVFSelected',uint16(wvf_num));
    end
    pause(pausetime);
    toc;
    if jj<ncorr
        for ii=1:58
            fprintf('\b');
        end
    end
end

tic;
fprintf('%s Corrector num ',CORFamily);
for jj=1:ncorr
    fprintf('%d/%d WvfScaling sent... ',jj,ncorr);
    list=dev_list(dev_list(:,2)==jj,1)';
    for ii=list
        kk=list_all_corr((dev_list(:,1)==ii)&(dev_list(:,2)==jj));
        tango_write_attribute(dev_names{kk},'WVFScaling',single(WvfScaling(kk)));
    end
    pause(pausetime);
    toc;
    if jj<ncorr
        for ii=1:57
            fprintf('\b');
        end
    end
end


end
