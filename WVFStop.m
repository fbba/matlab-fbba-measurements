%{
    Copyright (C) 2020  Zeus Martí Díaz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}

function WVFStop(corr_device)
%% Function Name: WVFStop
% WVFStop(corr_device)
%
% Assumptions: sent stop command to an a single DS
% also valid for tango binding before 2015
%
% Inputs:
%  corr_device: single tango device name of the PS
%
% Outputs:
%   None

%
% Date: Jan 30, 2020
% ________________________________________

ver=version; %Z.Marti modification 2018/03/09
yearver=str2double(ver((end-5):(end-2))); %Z.Marti modification 2018/03/09

if yearver>2015
    tango_command_inout(corr_device,'WVFStop',false);
else
    tango_command_inout(corr_device,'WVFStop',uint8(0));
end
end
