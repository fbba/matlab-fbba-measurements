%{
    Copyright (C) 2020  Zeus Martí Díaz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}

function [BPM_centre, BPM_error]=find_offsets(jbpm,Axa,Aya,Mxa,Mya,Axb,Ayb,Mxb,Myb,varargin)
% [BPM_centre, BPM_error]=find_offsets(jbpm,Axa,Aya,Mxa,Mya,Axb,Ayb,Mxb,Myb,PlotFalg,xstd,ystd,Ax0,Ay0,nusamp)
%
% Assumptions: Calculate the offsets using the fourier analysis results as
% imput. We assume that nfreq=3 (three frequencies).
%
% Inputs:
%  jbpm: index of the BPM next to the varied quadrupole, there are nbpm BPMs.
%  Axa: [nfreq X nbpm] Horizontal BPM reading amplitude before the quad/skew change at the used frequencies [mm]
%  Aya: [nfreq X nbpm] Vertical BPM reading amplitude before the quad/skew change at the used frequencies [mm] 
%  Mxa: [nfreq X nbpm] Horizontal BPM reading phase before the quad/skew change at the used frequencies [rad]
%  Mya: [nfreq X nbpm] Vertical BPM reading phase before the quad/skew change at the used frequencies [rad]
%  Axa: [nfreq X nbpm] Horizontal BPM reading amplitude after the quad/skew change at the used frequencies [mm]
%  Aya: [nfreq X nbpm] Vertical BPM reading amplitude after the quad/skew change at the used frequencies [mm] 
%  Mxa: [nfreq X nbpm] Horizontal BPM reading phase after the quad/skew change at the used frequencies [rad]
%  Mya: [nfreq X nbpm] Vertical BPM reading phase after the quad/skew change at the used frequencies [rad]
%  PlotFalg: Flag that enables the Mv and Mh fit plot {default:false}
%  xstd: [nbpm X 1] rms of the unperturbed orbit {default: 0.0}
%  ystd: [nbpm X 1] rms of the unperturbed orbit {default: 0.0}
%  Ax0: [nfreq X nbpm] Horizontal BPM reading amplitude of the unperturbed orbit at the used frequencies {default: 0.0}
%  Ay0: [nfreq X nbpm] VErtical BPM reading amplitude of the unperturbed orbit at the used frequencies {default: 0.0}
%  nusamp: number of Fast archiver samples used. {default: 1}

%
% Outputs:
%   BPM_centre: [1 X 2] Measured BPM offset in the two planes
%   BPM_error: [1 X 2] Measured BPM offset error in the two planes
%
% Date: Jan 30, 2020
% ________________________________________

PlotFalg=false;
xstd=zeros(size(Axa,2),1);
ystd=zeros(size(Axa,2),1);
Ax0=0*Axa;
Ay0=0*Aya;
nusamp=1;
if numel(varargin)==1
    PlotFalg=strcmpi('plot',varargin{1});
elseif numel(varargin)==6
    PlotFalg=strcmpi('plot',varargin{1});
    xstd=varargin{2};
    ystd=varargin{3};
    Ax0=varargin{4};
    Ay0=varargin{5}; 
    nusamp=varargin{6}; 
end

%%
Axa1(1,:)=2*Axa(1,:).*sign(cos(Mxa(1,:)));
Axb1(1,:)=2*Axb(1,:).*sign(cos(Mxb(1,:)));
Aya1(1,:)=2*Aya(1,:).*sign(cos(Mya(1,:)));
Ayb1(1,:)=2*Ayb(1,:).*sign(cos(Myb(1,:)));

[pha, phb]=findphases(Mxa(2,:),Mxb(2,:));
[pva, pvb]=findphases(Mya(2,:),Myb(2,:));

Axa1(2,:)=2*Axa(2,:).*sign(cos(Mxa(2,:)-pha));
Axb1(2,:)=2*Axb(2,:).*sign(cos(Mxb(2,:)-phb));
Aya1(2,:)=2*Aya(2,:).*sign(cos(Mya(2,:)-pva));
Ayb1(2,:)=2*Ayb(2,:).*sign(cos(Myb(2,:)-pvb));

Axa1(3,:)=2*Axa(3,:).*sign(cos(Mxa(3,:)-pva));
Axb1(3,:)=2*Axb(3,:).*sign(cos(Mxb(3,:)-pvb));
Aya1(3,:)=2*Aya(3,:).*sign(cos(Mya(3,:)-pha));
Ayb1(3,:)=2*Ayb(3,:).*sign(cos(Myb(3,:)-phb));
Dh0=Axb1(1,:)-Axa1(1,:);
Dv0=Ayb1(1,:)-Aya1(1,:);
Dhh=Axb1(2,:)-Axa1(2,:);
Dvh=Ayb1(3,:)-Aya1(3,:);
Dhv=Axb1(3,:)-Axa1(3,:);
Dvv=Ayb1(2,:)-Aya1(2,:);
Yh=-(Dh0.*Dvv-Dhv.*Dv0);
Xh=Dhh.*Dvv-Dhv.*Dvh;
Yv=-(Dv0.*Dhh-Dh0.*Dvh);
Xv=Xh;

[Cy,Sy]=polyfit(Xv,Yv,1);
[Cx,Sx]=polyfit(Xh,Yh,1);
covx=inv(Sx.R)*inv(Sx.R')*Sx.normr^2/Sx.df;
covy=inv(Sy.R)*inv(Sy.R')*Sy.normr^2/Sy.df;

x0a=Axa1(1,jbpm)+Axa1(2,jbpm)*Cx(1)+Axa1(3,jbpm)*Cy(1);
x0b=Axb1(1,jbpm)+Axb1(2,jbpm)*Cx(1)+Axb1(3,jbpm)*Cy(1);
y0a=Aya1(1,jbpm)+Aya1(3,jbpm)*Cx(1)+Aya1(2,jbpm)*Cy(1);
y0b=Ayb1(1,jbpm)+Ayb1(3,jbpm)*Cx(1)+Ayb1(2,jbpm)*Cy(1);
BPM_centre(1)=(x0a+x0b)/2;
BPM_centre(2)=(y0a+y0b)/2;
BPM_error(1)=sqrt(Axa1(2,jbpm)^2*covx(1,1)+Axa1(3,jbpm)^2*covy(1,1)+xstd(jbpm)^2/nusamp+(Ax0(2,jbpm)*Cx(1))^2+(Ax0(3,jbpm)*Cy(1))^2);
BPM_error(2)=sqrt(Aya1(3,jbpm)^2*covx(1,1)+Aya1(2,jbpm)^2*covy(1,1)+ystd(jbpm)^2/nusamp+(Ay0(3,jbpm)*Cx(1))^2+(Ay0(2,jbpm)*Cy(1))^2);


%%

if PlotFalg
figure;

    yfit=polyval(Cy,Xv);
    plot(1e6*Xv,1e6*Yv,'r.');hold all;
    plot(1e6*Xv,1e6*yfit,'-r');
    xlabel('$\mathcal{X}_{vk}$ [$\mu$m$^2$]','Interpreter','latex');
    ylabel('$\mathcal{Y}_{vk}$ [$\mu$m$^2$]','Interpreter','latex');  
    stri=sprintf('Fit $\\mathcal{M}_v$=(%1.1f$\\pm$%1.1f)$\\times 10^{-3}$',1e3*Cy(1),1e3*sqrt(covy(1,1)));
    %stri=sprintf('Fit $\\mathcal{M}_v$=(%1.1f$\\pm$%1.1f)',Cy(1),sqrt(covy(1,1)));
    if Cy(1)>0
        legend({'Data',stri},'Interpreter','latex');
        legend('Location','NorthWest');
    else
        legend({'Data',stri},'Interpreter','latex');
        legend('Location','NorthEast');
    end
    yl=ylim;
    SetPlotSize(gcf,10,6);
    print('quadDC_proj_fit_Mv.png','-dpng','-r600');
    
    figure;

    yfit=polyval(Cx,Xh);
    plot(1e6*Xh,1e6*Yh,'b.');hold all;
    plot(1e6*Xh,1e6*yfit,'-b');
    xlabel('$\mathcal{X}_{hk}$ [$\mu$m$^2$]','Interpreter','latex');
    ylabel('$\mathcal{Y}_{hk}$ [$\mu$m$^2$]','Interpreter','latex');  
    stri=sprintf('Fit $\\mathcal{M}_h$=(%1.1f$\\pm$%1.1f)$\\times 10^{-3}$',1e3*Cx(1),1e3*sqrt(covx(1,1)));
    %stri=sprintf('Fit $\\mathcal{M}_h$=(%1.1f$\\pm$%1.1f)',Cx(1),sqrt(covx(1,1)));
    if Cx(1)>0
        legend({'Data',stri},'Interpreter','latex');
        legend('Location','NorthWest');
    else
        legend({'Data',stri},'Interpreter','latex');
        legend('Location','NorthEast');
    end
    ylim(yl);
    SetPlotSize(gcf,10,6);
    print('quadDC_proj_fit_Mh.png','-dpng','-r600');
    
    
end

end

function [pa pb]=findphases(ma,mb)


m_sum=mod(ma+mb+pi,2*pi)-pi;
good=abs(m_sum-mean(m_sum))<2*std(m_sum);
phi_sum=mean(m_sum(good));

m_dif=mod(ma-mb+pi,2*pi)-pi;
good=abs(m_dif-mean(m_dif))<2*std(m_dif);
phi_dif=mean(m_dif(good));

pa=(phi_sum+phi_dif)/2;
pb=(phi_sum-phi_dif)/2;
end
