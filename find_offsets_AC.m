%{
    Copyright (C) 2020  Zeus Martí Díaz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}

function [BPM_centre, BPM_error,ps]=find_offsets_AC(jbpm,Axini,Ayini,Mx,My,varargin)
%% Function Name: find_offsets_AC
% [BPM_centre, BPM_error]=find_offsets_AC(jbpm,Axini,Ayini,Mx,My,PlotFalg,xstd,ystd,Ax0,Ay0,nusamp)
%
% Assumptions: Calculate the offsets using the fourier analysis results as
% imput. We assume that nfreq=8 (eight frequencies).
%
% Inputs:
%  jbpm: index of the BPM next to the varied quadrupole, there are nbpm BPMs.
%  Axini: [nfreq X nbpm] Horizontal BPM reading amplitude during the quad/skew change at the used frequencies [mm]
%  Ayini: [nfreq X nbpm] Vertical BPM reading amplitude during the quad/skew change at the used frequencies [mm] 
%  Mx: [nfreq X nbpm] Horizontal BPM reading phase during the quad/skew change at the used frequencies [rad]
%  My: [nfreq X nbpm] Vertical BPM reading phase during the quad/skew change at the used frequencies [rad]
%  PlotFalg: Flag that enables the Mv and Mh fit plot {default:false}
%  xstd: [nbpm X 1] rms of the unperturbed orbit {default: 0.0}
%  ystd: [nbpm X 1] rms of the unperturbed orbit {default: 0.0}
%  Ax0: [nfreq X nbpm] Horizontal BPM reading amplitude of the unperturbed orbit at the used frequencies {default: 0.0}
%  Ay0: [nfreq X nbpm] VErtical BPM reading amplitude of the unperturbed orbit at the used frequencies {default: 0.0}
%  nusamp: number of Fast archiver samples used. {default: 1}
%
% Outputs:
%   BPM_centre: [1 X 2] Measured BPM offset in the two planes
%   BPM_error: [1 X 2] Measured BPM offset error in the two planes
%
% Date: Jan 30, 2020
% ________________________________________


PlotFalg=false;
xstd=zeros(size(Axini,2),1);
ystd=zeros(size(Axini,2),1);
Ax0=0*Axini;
Ay0=0*Ayini;
if numel(varargin)==1
    PlotFalg=strcmpi('plot',varargin{1});
elseif numel(varargin)==6
    PlotFalg=strcmpi('plot',varargin{1});
    xstd=varargin{2};
    ystd=varargin{3};
    Ax0=varargin{4};
    Ay0=varargin{5}; 
    nusamp=varargin{6};
end

%%
Ax=Axini;
Ay=Ayini;

Ax(1,:)=2*Axini(1,:).*sign(cos(Mx(1,:)));
Ay(1,:)=2*Ayini(1,:).*sign(cos(My(1,:)));


[ph,pv,ps]=findphases(Mx,My);

%disp([ph,pv,ps]/pi*180)

Ax(2,:)=2*Axini(2,:).*sign(cos(Mx(2,:)-ph));
Ay(2,:)=2*Ayini(2,:).*sign(cos(My(2,:)-pv));

Ax(3,:)=2*Axini(3,:).*sign(cos(Mx(3,:)-pv));
Ay(3,:)=2*Ayini(3,:).*sign(cos(My(3,:)-ph));

Ax(4,:)=2*Axini(4,:).*sign(cos(Mx(4,:)-ps));
Ay(4,:)=2*Ayini(4,:).*sign(cos(My(4,:)-ps));

Ax(5,:)=2*Axini(5,:).*sign(cos(Mx(5,:)-pv-ps));
Ay(5,:)=2*Ayini(5,:).*sign(cos(My(5,:)-ph-ps));

Ax(6,:)=2*Axini(6,:).*sign(cos(Mx(6,:)-pv+ps));
Ay(6,:)=2*Ayini(6,:).*sign(cos(My(6,:)-ph+ps));

Ax(7,:)=2*Axini(7,:).*sign(cos(Mx(7,:)-ph-ps));
Ay(7,:)=2*Ayini(7,:).*sign(cos(My(7,:)-pv-ps));

Ax(8,:)=2*Axini(8,:).*sign(cos(Mx(8,:)-ph+ps));
Ay(8,:)=2*Ayini(8,:).*sign(cos(My(8,:)-pv+ps));

Ahs=Ax(4,:);
Avs=Ay(4,:);
Ahhs=Ax(7,:)+Ax(8,:);
Avhs=Ay(5,:)+Ay(6,:);
Ahvs=Ax(5,:)+Ax(6,:);
Avvs=Ay(7,:)+Ay(8,:);
Yh=-(Ahs.*Avvs-Ahvs.*Avs);
Xh=Ahhs.*Avvs-Ahvs.*Avhs;
Yv=-(Avs.*Ahhs-Ahs.*Avhs);
Xv=Xh;


[Cx,Sx]=polyfit(Xh,Yh,1);
[Cy,Sy]=polyfit(Xv,Yv,1);

covx=inv(Sx.R)*inv(Sx.R')*Sx.normr^2/Sx.df;
covy=inv(Sy.R)*inv(Sy.R')*Sy.normr^2/Sy.df;

BPM_centre(1)=Ax(1,jbpm)+Ax(2,jbpm)*Cx(1)+Ax(3,jbpm)*Cy(1);
BPM_centre(2)=Ay(1,jbpm)+Ay(2,jbpm)*Cy(1)+Ay(3,jbpm)*Cx(1);
BPM_error(1)=sqrt(Ax(2,jbpm)^2*covy(1,1)+Ax(3,jbpm)^2*covx(1,1)+xstd(jbpm)^2/nusamp+(Ax0(2,jbpm)*Cx(1))^2+(Ax0(3,jbpm)*Cy(1))^2);
BPM_error(2)=sqrt(Ay(2,jbpm)^2*covx(1,1)+Ay(3,jbpm)^2*covy(1,1)+ystd(jbpm)^2/nusamp+(Ay0(3,jbpm)*Cx(1))^2+(Ay0(2,jbpm)*Cy(1))^2);
%%

if PlotFalg
    figure;
    fit=polyval(Cx,Xh);
    plot(1e6*Xh,1e6*Yh,'b.');hold all;
    plot(1e6*Xh,1e6*fit,'-b');
    
    xlabel('$\mathcal{X}_{hk}$ [$\mu$m$^2$]','Interpreter','latex');
    ylabel('$\mathcal{Y}_{hk}$ [$\mu$m$^2$]','Interpreter','latex');  
    stri=sprintf('Fit $\\mathcal{M}_h$=(%1.1f$\\pm$%1.1f)$\\times 10^{-3}$',1e3*Cx(1),1e3*sqrt(covx(1,1)));
    if Cx(1)>0
        legend({'Data',stri},'Interpreter','latex');
        legend('Location','NorthWest');
    else
        legend({'Data',stri},'Interpreter','latex');
        legend('Location','NorthEast');
    end
    SetPlotSize(gcf,10,6);
    print('skew_proj_fit_Mh.png','-dpng','-r600');
    
    figure;


    fit=polyval(Cy,Xv);
    plot(1e6*Xv,1e6*Yv,'r.');hold all;
    plot(1e6*Xv,1e6*fit,'-r');
    xlabel('$\mathcal{X}_{vk}$ [$\mu$m$^2$]','Interpreter','latex');
    ylabel('$\mathcal{Y}_{hk}$ [$\mu$m$^2$]','Interpreter','latex');  
    stri=sprintf('Fit $\\mathcal{M}_v$=(%1.1f$\\pm$%1.1f)$\\times 10^{-3}$',1e3*Cy(1),1e3*sqrt(covy(1,1)));
    if Cy(1)>0
        legend({'Data',stri},'Interpreter','latex');
        legend('Location','NorthWest');
    else
        legend({'Data',stri},'Interpreter','latex');
        legend('Location','NorthEast');
    end
    SetPlotSize(gcf,10,6);
    print('skew_proj_fit_Mv.png','-dpng','-r600');
end


end

function [phi_h,phi_v,phi_s]=findphases(Mx,My)


m_h=(mod(2*Mx(2,:),2*pi))/2;
good=abs(m_h-mean(m_h))<2*std(m_h);
phi_h=mean(m_h(good));

m_v=(mod(2*My(2,:),2*pi))/2;
good=abs(m_v-mean(m_v))<2*std(m_v);
phi_v=mean(m_v(good));

m_s(1,:)=(mod(2*(My(5,:)-phi_h),2*pi))/2;
m_s(2,:)=(mod(2*(phi_h-My(6,:)),2*pi))/2;
m_s(3,:)=(mod(2*(Mx(5,:)-phi_v),2*pi))/2;
m_s(4,:)=(mod(2*(phi_v-Mx(6,:)),2*pi))/2;
m_s(5,:)=(mod(2*Mx(4,:),2*pi))/2;
m_s(6,:)=(mod(2*My(4,:),2*pi))/2;
m_s(7,:)=(mod(2*(My(7,:)-phi_v),2*pi))/2;
m_s(8,:)=(mod(2*(phi_v-My(8,:)),2*pi))/2;
m_s(9,:)=(mod(2*(Mx(7,:)-phi_h),2*pi))/2;
m_s(10,:)=(mod(2*(phi_h-Mx(8,:)),2*pi))/2;

stds=std(m_s,[],2);
good=stds<(3*min(stds));
phi_s=mean(mean(m_s(good,:),2));
end
