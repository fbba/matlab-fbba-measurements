%{
    Copyright (C) 2020  Zeus Martí Díaz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}

%% Prepare amplitude and frequency arrays of CMs and skews

albainit('AP');
fh=6.9464*ones(88,1); % calibrated 1e6/(400*360)
fv=5.9537*ones(88,1); % calibrated 1e6/(400*420)
%fskew=4.0984*ones(32,1)/2.5; % 1e6/(400*810)
fskew=4.0984*ones(32,1); % 1e6/(400*810)

WvfScaling_H=0.4*ones(88,1);
WvfScaling_V=0.4*ones(88,1);
WvfScaling_S=2.5*ones(32,1);%ones(32,1);%

f_s=9957.9240636;
wvf_num=9;
wvf_num_skew=1;
wvfstepsize=80;
wvfstepsize_skew=80*2.5;
%wvfstepsize_skew=80;
corr_device='sr01/pc/corv-02';
quad_dev='sr10/pc/qh07';

nuseconds=5;
load('FaIDs.mat');
foldercalib='/data/MML/WorkDirectory/zeus/FBBA/measurements/';

ao=getao;
GroupId_VCM=ao.VCM.GroupId;
GroupId_HCM=ao.HCM.GroupId;
GroupId_QS=ao.QS.GroupId;


%% Preparation sinusoidal waveforms
% stop all waveforms for safety
WVFStop_group(GroupId_VCM);
WVFStop_group(GroupId_HCM);
WVFStop_group(GroupId_QS);
pause(1);

WVFPrepare('VCM',wvf_num,WvfScaling_V,wvfstepsize);
WVFPrepare('HCM',wvf_num,WvfScaling_H,wvfstepsize);
WVFPrepare('QS',wvf_num_skew,WvfScaling_S,wvfstepsize_skew);


%% BPM quad list

global THERING;
ati=atindex(THERING);
s=findspos(THERING,1:(numel(THERING)+1));
ao=getao;

R0=getbpmresp('Struct','Physics');
Rx=R0(1,1).Data;
Ry=R0(2,2).Data;

qf=findmemberof('QUAD');
qind=[];
Qdevs={};
for ii=1:numel(qf)
    qind=[qind ati.(qf{ii})];
    Qdevs=[Qdevs; ao.(qf{ii}).DeviceName];
end

sbpm=s(ati.BPM);
[sort_qind, iqsort]=sort(qind);
squad=s(sort_qind);
Qdevs=Qdevs(iqsort,:);

nbpm=numel(sbpm);
nq=numel(squad);
dist=sbpm(:)*ones(1,nq)-ones(nbpm,1)*squad(:)';
[md, iq]=min(abs(dist),[],2);
quads_names=Qdevs(iq,:);

[mx, icmx]=max(abs(Rx),[],2);
[my, icmy]=max(abs(Ry),[],2);

hcm_names=ao.HCM.DeviceName(icmx);
vcm_names=ao.VCM.DeviceName(icmy);

bpm_devices=ao.BPMx.DeviceList;
bpm_names=ao.BPMx.DeviceName;
% quads_names=quads_names(:)';
% hcm_names=hcm_names(:)';
% vcm_names=vcm_names(:)';
dev_quads_names=quads_names;
dev_hcm_names=hcm_names;
dev_vcm_names=vcm_names;
quads_names=cell2mat(dev_quads_names);
hcm_names=cell2mat(dev_hcm_names);
vcm_names=cell2mat(dev_vcm_names);
save('/data/MML/WorkDirectory/zeus/FBBA/fbba_data.mat','quads_names','hcm_names','vcm_names','bpm_devices','-v7');


