#!/usr/bin/env python

"""
    Copyright (C) 2020  Zeus Martí Díaz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import PyTango
import sys


listargs=sys.argv
WVFStepSize = [int(i) for i in listargs[1].split()]
devname=listargs[2:]
ndevs=len(devname)
nsz=len(WVFStepSize)

if nsz!=ndevs:
    if nsz!=1:
        print("Oops!  No valid number of arguments.  Try again...")
        sys.exit()
    
if WVFStepSize>=[80]:
    for ii in range(0,ndevs):
        dev=PyTango.DeviceProxy(devname[ii])
        if nsz==1:
            dev.WVFStepSize=WVFStepSize[0]
        else:
            dev.WVFStepSize=WVFStepSize[ii]
            
else:
     print("Oops!  That was no valid number.  Try again...")
    



  
