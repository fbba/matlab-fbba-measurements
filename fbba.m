%{
    Copyright (C) 2020  Zeus Martí Díaz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}


%% Preparin HCMs and VCMs waveform parameters for FBBA
% also the list of the closest quadrupoles and most effcient corrector
% magnet (CM) is generated here.
% This part is very ALBA setup dependent

fprintf('\nFBBA mode preparation follows...\n\n');
preparation_fbba;
addpath('/data/Diagnostics/angel/Matlabfiles/');

%% FBBA measurement
% select which BPMs you want to analyze by changing list_bpms:
list_bpms=1:numel(bpm_devices);
% select the number of fast archiver samples by changin nusamp:
nusamp=15054;


QUADS_INITIAL=getpv(findmemberof('QUAD'));
meas_folder='/data/MML/WorkDirectory/zeus/FBBA/measurements/';
timestamp=datenum(clock);
ara=datestr(timestamp,'mmm_dd_yyyy-HH-MM-SS');

nbpm_toanalyse=numel(list_bpms);
BPM_centre=zeros(nbpm_toanalyse,2);
BPM_error=zeros(nbpm_toanalyse,2);
elapsed_time=zeros(nbpm_toanalyse,1);
analysis_time=zeros(nbpm_toanalyse,1);

% calibrated values (ALBA specific)
fh=6.9464;
fv=5.9537;

dev_used_quads_names=dev_quads_names(list_bpms);
dev_used_hcm_names=dev_hcm_names(list_bpms);
dev_used_vcm_names=dev_vcm_names(list_bpms);
bpm_used_names=bpm_names(list_bpms);

% First acquisition without any excitation (needed to evaluate measurement errors)
data=fa_load(nusamp,FaIDs,'C','idifa01');
f_s=data.f_s;
x0=squeeze(data.data(1,:,:)*1e-6);
y0=squeeze(data.data(2,:,:)*1e-6);
xstd=std(x0,[],2);
ystd=std(y0,[],2);
[Ax0, Mx0]=tune_search_mproj([0 fh/f_s fv/f_s],x0');
[Ay0, My0]=tune_search_mproj([0 fv/f_s fh/f_s],y0');
nbpmsfa=size(Ax0,2);    
fhv=[0 fh/f_s fv/f_s];
fvh=[0 fv/f_s fh/f_s];
nfs=numel(fhv);
clear x0 y0;

Axa=zeros(nfs,nbpmsfa,nbpm_toanalyse);
Axb=zeros(nfs,nbpmsfa,nbpm_toanalyse);
Aya=zeros(nfs,nbpmsfa,nbpm_toanalyse);
Ayb=zeros(nfs,nbpmsfa,nbpm_toanalyse);
Mxa=zeros(nfs,nbpmsfa,nbpm_toanalyse);
Mxb=zeros(nfs,nbpmsfa,nbpm_toanalyse);
Mya=zeros(nfs,nbpmsfa,nbpm_toanalyse);
Myb=zeros(nfs,nbpmsfa,nbpm_toanalyse);

% select a particular BPM to store all the raw data
selected_raw_for_bpm=0;

fprintf('----------projection FBBA both planes ------------\n');
for ibpm=1:nbpm_toanalyse
    % Measurement part:
    tic;
    jbpm=list_bpms(ibpm);
    st_quad=tango_read_attribute(dev_quads_names{jbpm},'CurrentSetpoint');
    Iq0=st_quad.value(1);
    tango_write_attribute(dev_quads_names{jbpm},'CurrentSetpoint',Iq0-QuadDelta);
    WVFStartAuto(dev_hcm_names{jbpm});
    WVFStartAuto(dev_vcm_names{jbpm});
    pause(1.5);% avoid transient behaviour (ALBA dependent)
    data=fa_load(nusamp,FaIDs,'C','idifa01');
    xa=squeeze(data.data(1,:,:)*1e-6);
    ya=squeeze(data.data(2,:,:)*1e-6);
    tango_write_attribute(dev_quads_names{jbpm},'CurrentSetpoint',Iq0+QuadDelta);
    pause(1.5);% avoid transient behaviour (ALBA dependent)
    data=fa_load(nusamp,FaIDs,'C','idifa01');
    xb=squeeze(data.data(1,:,:)*1e-6);
    yb=squeeze(data.data(2,:,:)*1e-6);
    tango_write_attribute(dev_quads_names{jbpm},'CurrentSetpoint',Iq0);
    WVFStop(dev_hcm_names{jbpm});
    WVFStop(dev_vcm_names{jbpm});
    if ibpm==selected_raw_for_bpm
        xa_selected=xa;
        ya_selected=ya;
        xb_selected=xb;
        yb_selected=yb;
    end
    elapsed_time(ibpm)=toc;
    
    % amplitudes and phases analysis
    [Axa(:,:,ibpm), Mxa(:,:,ibpm)]=tune_search_mproj(fhv,xa');
    [Axb(:,:,ibpm), Mxb(:,:,ibpm)]=tune_search_mproj(fhv,xb');
    [Aya(:,:,ibpm), Mya(:,:,ibpm)]=tune_search_mproj(fvh,ya');
    [Ayb(:,:,ibpm), Myb(:,:,ibpm)]=tune_search_mproj(fvh,yb');
    

    [BPM_centre(ibpm,:), BPM_error(ibpm,:)]=find_offsets(jbpm,Axa(:,:,ibpm),Aya(:,:,ibpm),Mxa(:,:,ibpm),Mya(:,:,ibpm),Axb(:,:,ibpm),Ayb(:,:,ibpm),Mxb(:,:,ibpm),Myb(:,:,ibpm));
    total_elapsed_time=toc;
    
    analysis_time(ibpm)=total_elapsed_time-elapsed_time(ibpm);
    fprintf('    BPMx %d, %1.3f +/- %1.3f mm (%1.3f +/- %1.3f), ',ibpm,BPM_centre(ibpm,1),BPM_error(ibpm,1));
    fprintf('    BPMy %d, %1.3f +/- %1.3f mm (%1.3f +/- %1.3f), ',ibpm,BPM_centre(ibpm,2),BPM_error(ibpm,2));
    
    toc;
end

clear data xa xb ya yb;

% return all quadrupoles to initial values 
%(just in case there has been an unnoticed error during the measurement)
pause(3);
setpv(findmemberof('QUAD'),QUADS_INITIAL);


file_data=[meas_folder 'FBBA_proj_' ara '.mat'];
if selected_raw_for_bpm>0&&selected_raw_for_bpm<120
    save(file_data,'f_s','fh','fv','BPM_centre','BPM_error','analysis_time','elapsed_time','xstd','ystd','list_bpms','QuadDelta',...
        'timestamp','dev_used_quads_names','dev_used_hcm_names','dev_used_vcm_names','bpm_used_names',...
        'xa_selected','xb_selected','ya_selected','yb_selected','selected_raw_for_bpm',...
        'xstd','ystd','Axa','Aya','Mxa','Mya','Axb','Ayb','Mxb','Myb','Ax0','Ay0','Mx0','My0','nusamp');
else
    save(file_data,'f_s','fh','fv','BPM_centre','BPM_error','analysis_time','elapsed_time','xstd','ystd','list_bpms','QuadDelta',...
        'timestamp','dev_used_quads_names','dev_used_hcm_names','dev_used_vcm_names','bpm_used_names',...
        'xstd','ystd','Axa','Aya','Mxa','Mya','Axb','Ayb','Mxb','Myb','Ax0','Ay0','Mx0','My0','nusamp');
end

fprintf('All data has been saved to:\n %s\n\n',file_data);

%% Return to FRM settings
% Returns VCMs and HCMs to their default waveform settings
% This part is very ALBA setup dependent

fprintf('\nFRM mode preparation follows...\n\n');
preparation_frm;
