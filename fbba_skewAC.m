%{
    Copyright (C) 2020  Zeus Martí Díaz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}

%% Preparin HCMs, VCMs and skews waveform parameters for FBBA
% also the list of the closest skew and most effcient correctors
% magnet (HCM and VCM) is generated here.
% This part is very ALBA setup dependent

fprintf('\nFBBA mode preparation follows...\n\n');
preparation_skewAC_fbba;
addpath('/data/Diagnostics/angel/Matlabfiles/');
meas_folder='/data/MML/WorkDirectory/zeus/FBBA/measurements/';

%% FBBA proj vesion
QUADS_INITIAL=getpv(findmemberof('SkewQuad'));
timestamp=datenum(clock);
ara=datestr(timestamp,'mmm_dd_yyyy-HH-MM-SS');
nusamp=64447;% optimum 4.09 Hz
dev_qs_names=ao.QS.DeviceName;

devsquad1=[1 3; 1 5;2 3;2 6 ;3 3;3 6;4 3; 4 5];
devsquad=repmat(devsquad1,[4 1]);
addsector=[zeros(8,2);4*ones(8,1) zeros(8,1);8*ones(8,1) zeros(8,1);12*ones(8,1) zeros(8,1)];
list_bpms=dev2elem('BPMx',devsquad+addsector);
%list_bpms=list_bpms(1);

nbpm_toanalyse=numel(list_bpms);
BPM_centre=zeros(nbpm_toanalyse,2);
BPM_error=zeros(nbpm_toanalyse,2);
elapsed_time=zeros(nbpm_toanalyse,1);
anal_time=zeros(nbpm_toanalyse,1);

dev_used_quads_names=dev_quads_names(list_bpms);
dev_used_hcm_names=dev_hcm_names(list_bpms);
dev_used_vcm_names=dev_vcm_names(list_bpms);
bpm_used_names=bpm_names(list_bpms);


% calibrated values
fh=6.9464;
fv=5.9537;
fskew=4.0984/2.5;
data=fa_load(nusamp,FaIDs,'C','idifa01');
f_s=data.f_s;
fhv=[0 fh/f_s fv/f_s fskew/f_s (fv+fskew)/f_s abs(fv-fskew)/f_s (fh+fskew)/f_s abs(fh-fskew)/f_s];
fvh=[0 fv/f_s fh/f_s fskew/f_s (fh+fskew)/f_s abs(fh-fskew)/f_s (fv+fskew)/f_s abs(fv-fskew)/f_s];
x0=squeeze(data.data(1,:,:)*1e-6);
y0=squeeze(data.data(2,:,:)*1e-6);
xstd=std(x0,[],2);
ystd=std(y0,[],2);
[Ax0, Mx0]=tune_search_mproj(fhv,x0');
[Ay0, My0]=tune_search_mproj(fvh,y0');
nbpmsfa=size(Ax0,2);    
nfs=numel(fhv);
clear x0 y0;
Ax=zeros(nfs,nbpmsfa,nbpm_toanalyse);
Ay=zeros(nfs,nbpmsfa,nbpm_toanalyse);
Mx=zeros(nfs,nbpmsfa,nbpm_toanalyse);
My=zeros(nfs,nbpmsfa,nbpm_toanalyse);

   
selected_raw_for_bpm=0;%nbpm_toanalyse+1;
fprintf('----------projection FBBA both planes ------------\n');
for ibpm=1:nbpm_toanalyse
    tic;
    jbpm=list_bpms(ibpm);

    WVFStartAuto(dev_qs_names{ibpm});
    WVFStartAuto(dev_hcm_names{jbpm});
    WVFStartAuto(dev_vcm_names{jbpm});
    pause(0.5);% avoid transient behaviour
    data=fa_load(nusamp,FaIDs,'C','idifa01');
    xa=squeeze(data.data(1,:,:)*1e-6);
    ya=squeeze(data.data(2,:,:)*1e-6);
    if ibpm==selected_raw_for_bpm
        xa_selected=xa;
        ya_selected=ya;
    end
    
    WVFStop(dev_hcm_names{jbpm});
    WVFStop(dev_vcm_names{jbpm});
    WVFStop(dev_qs_names{ibpm});
    
    elapsed_time(ibpm)=toc;
    [Ax(:,:,ibpm), Mx(:,:,ibpm)]=tune_search_mproj(fhv,xa');
    [Ay(:,:,ibpm), My(:,:,ibpm)]=tune_search_mproj(fvh,ya');
    
    [BPM_centre(ibpm,:), BPM_error(ibpm,:)]=find_offsets_AC(jbpm,Ax(:,:,ibpm),Ay(:,:,ibpm),Mx(:,:,ibpm),My(:,:,ibpm));
    
    if selected_raw_for_bpm>nbpm_toanalyse
        save([meas_folder 'FBBA_skew_AC_' ara '_case_' int2str(ibpm) '.mat'],'BPM_centre','BPM_error','list_bpms','dev_qs_names',...
            'QuadDelta','timestamp','dev_used_quads_names','dev_used_hcm_names','dev_used_vcm_names','bpm_used_names',...
            'f_s','xa','ya','selected_raw_for_bpm','fh','fv','fskew');
    end

    
    total_elapsed_time=toc;
    anal_time(ibpm)=total_elapsed_time-elapsed_time(ibpm);
    fprintf('    BPMx %d, %1.3f +/- %1.3f mm (%1.3f +/- %1.3f), ',ibpm,BPM_centre(ibpm,1),BPM_error(ibpm,1),BPM_centre_CIA(ibpm,1),BPM_error_CIA(ibpm,1));
    fprintf('    BPMy %d, %1.3f +/- %1.3f mm, (%1.3f +/- %1.3f), ',ibpm,BPM_centre(ibpm,2),BPM_error(ibpm,2),BPM_centre_CIA(ibpm,2),BPM_error_CIA(ibpm,2));
    

    toc;
end

clear data xa ya;
setpv(findmemberof('SkewQuad'),QUADS_INITIAL);


if selected_raw_for_bpm>0&&selected_raw_for_bpm<32
    save([meas_folder 'FBBA_skew_AC_' ara '.mat'],'BPM_centre','BPM_error','anal_time','elapsed_time','list_bpms','dev_qs_names',...
        'QuadDelta','timestamp','dev_used_quads_names','dev_used_hcm_names','dev_used_vcm_names','bpm_used_names',...
        'f_s','xa_selected','ya_selected','selected_raw_for_bpm','fh','fv','fskew',...
        'xstd','ystd','Ax','Ay','Mx','My','Ax0','Ay0','Mx0','My0','nusamp');
else
    save([meas_folder 'FBBA_skew_AC_' ara '.mat'],'BPM_centre','BPM_error','anal_time','elapsed_time','list_bpms','dev_qs_names',...
        'QuadDelta','timestamp','dev_used_quads_names','dev_used_hcm_names','dev_used_vcm_names','bpm_used_names',...
        'f_s','selected_raw_for_bpm','fh','fv','fskew',...
        'xstd','ystd','Ax','Ay','Mx','My','Ax0','Ay0','Mx0','My0','nusamp');
end


%% Return to FRM settings
fprintf('\nFRM mode preparation follows...\n\n');
preparation_frm;


