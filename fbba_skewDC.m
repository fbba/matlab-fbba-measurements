%{
    Copyright (C) 2020  Zeus Martí Díaz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}


fprintf('\nFBBA mode preparation follows...\n\n');
preparation_fbba;
addpath('/data/Diagnostics/angel/Matlabfiles/');
meas_folder='/data/MML/WorkDirectory/zeus/FBBA/measurements/';

%% FBBA proj vesion
% overwrite QuadDelta here:
QuadDelta=2.5;

QUADS_INITIAL=getpv(findmemberof('SkewQuad'));

timestamp=datenum(clock);
ara=datestr(timestamp,'mmm_dd_yyyy-HH-MM-SS');
nusamp=64447;% optimum 4.09 Hz
dev_qs_names=ao.QS.DeviceName;

devsquad1=[1 3; 1 5;2 3;2 6 ;3 3;3 6;4 3; 4 5];
devsquad=repmat(devsquad1,[4 1]);
addsector=[zeros(8,2);4*ones(8,1) zeros(8,1);8*ones(8,1) zeros(8,1);12*ones(8,1) zeros(8,1)];
list_bpms=dev2elem('BPMx',devsquad+addsector);

nbpm_toanalyse=numel(list_bpms);
BPM_centre=zeros(nbpm_toanalyse,2);
BPM_error=zeros(nbpm_toanalyse,2);
elapsed_time=zeros(nbpm_toanalyse,1);
anal_time=zeros(nbpm_toanalyse,1);


% calibrated values
fh=6.9464;
fv=5.9537;

data=fa_load(nusamp,FaIDs,'C','idifa01');
f_s=data.f_s;
x0=squeeze(data.data(1,:,:)*1e-6);
y0=squeeze(data.data(2,:,:)*1e-6);
xstd=std(x0,[],2);
ystd=std(y0,[],2);
fhv=[0 fh/f_s fv/f_s];
fvh=[0 fv/f_s fh/f_s];
[Ax0, Mx0]=tune_search_mproj(fhv,x0');
[Ay0, My0]=tune_search_mproj(fvh,y0');
nbpmsfa=size(Ax0,2);    

nfs=numel(fhv);
clear x0 y0;
Axa=zeros(nfs,nbpmsfa,nbpm_toanalyse);
Axb=zeros(nfs,nbpmsfa,nbpm_toanalyse);
Aya=zeros(nfs,nbpmsfa,nbpm_toanalyse);
Ayb=zeros(nfs,nbpmsfa,nbpm_toanalyse);
Mxa=zeros(nfs,nbpmsfa,nbpm_toanalyse);
Mxb=zeros(nfs,nbpmsfa,nbpm_toanalyse);
Mya=zeros(nfs,nbpmsfa,nbpm_toanalyse);
Myb=zeros(nfs,nbpmsfa,nbpm_toanalyse);


dev_used_quads_names=dev_quads_names(list_bpms);
dev_used_hcm_names=dev_hcm_names(list_bpms);
dev_used_vcm_names=dev_vcm_names(list_bpms);
bpm_used_names=bpm_names(list_bpms);


selected_raw_for_bpm=9;
fprintf('----------projection FBBA both planes ------------\n');
for ibpm=1:nbpm_toanalyse
    tic;
    jbpm=list_bpms(ibpm);
    jbpm=list_bpms(ibpm);
    st_quad=tango_read_attribute(dev_qs_names{ibpm},'CurrentSetpoint');
    Iq0=st_quad.value(1);
    tango_write_attribute(dev_qs_names{ibpm},'CurrentSetpoint',Iq0-QuadDelta);
    WVFStartAuto(dev_hcm_names{jbpm});
    WVFStartAuto(dev_vcm_names{jbpm});
    pause(1.5);% avoid transient behaviour
    data=fa_load(nusamp,FaIDs,'C','idifa01');
    xa=squeeze(data.data(1,:,:)*1e-6);
    ya=squeeze(data.data(2,:,:)*1e-6);
    tango_write_attribute(dev_qs_names{ibpm},'CurrentSetpoint',Iq0+QuadDelta);
    pause(1.5);% avoid transient behaviour
    data=fa_load(nusamp,FaIDs,'C','idifa01');
    f_s=data.f_s;
    xb=squeeze(data.data(1,:,:)*1e-6);
    yb=squeeze(data.data(2,:,:)*1e-6);
    tango_write_attribute(dev_qs_names{ibpm},'CurrentSetpoint',Iq0);
    WVFStop(dev_hcm_names{jbpm});
    WVFStop(dev_vcm_names{jbpm});
    
    if ibpm==selected_raw_for_bpm
        xa_selected=xa;
        ya_selected=ya;
        xb_selected=xb;
        yb_selected=yb;
    end

    elapsed_time(ibpm)=toc;
    
    [Axa(:,:,ibpm), Mxa(:,:,ibpm)]=tune_search_mproj(fhv,xa');
    [Axb(:,:,ibpm), Mxb(:,:,ibpm)]=tune_search_mproj(fhv,xb');
    [Aya(:,:,ibpm), Mya(:,:,ibpm)]=tune_search_mproj(fvh,ya');
    [Ayb(:,:,ibpm), Myb(:,:,ibpm)]=tune_search_mproj(fvh,yb');
    
    [BPM_centre(ibpm,:), BPM_error(ibpm,:)]=find_offsets(jbpm,Axa(:,:,ibpm),Aya(:,:,ibpm),Mxa(:,:,ibpm),Mya(:,:,ibpm),Axb(:,:,ibpm),Ayb(:,:,ibpm),Mxb(:,:,ibpm),Myb(:,:,ibpm));

    clear data xa xb ya yb;
    
    total_elapsed_time=toc;
    anal_time(ibpm)=total_elapsed_time-elapsed_time(ibpm);
    fprintf('    BPMx %d, %1.3f +/- %1.3f mm (%1.3f +/- %1.3f), ',ibpm,BPM_centre(ibpm,1),BPM_error(ibpm,1));
    fprintf('    BPMy %d, %1.3f +/- %1.3f mm, (%1.3f +/- %1.3f), ',ibpm,BPM_centre(ibpm,2),BPM_error(ibpm,2));
    
    toc;
end
setpv(findmemberof('SkewQuad'),QUADS_INITIAL);
if selected_raw_for_bpm>0&&selected_raw_for_bpm<32
    save([meas_folder 'FBBA_skew_DC_' ara '.mat'],'BPM_centre','BPM_error','anal_time','elapsed_time','list_bpms','dev_qs_names',...
        'QuadDelta','timestamp','dev_used_quads_names','dev_used_hcm_names','dev_used_vcm_names','bpm_used_names',...
        'f_s','xa_selected','xb_selected','ya_selected','yb_selected','selected_raw_for_bpm','fh','fv',...
        'xstd','ystd','Axa','Aya','Mxa','Mya','Axb','Ayb','Mxb','Myb','Ax0','Ay0','Mx0','My0','nusamp');
else
    save([meas_folder 'FBBA_skew_DC_' ara '.mat'],'BPM_centre','BPM_error','anal_time','elapsed_time','list_bpms','dev_qs_names',...
        'QuadDelta','timestamp','dev_used_quads_names','dev_used_hcm_names','dev_used_vcm_names','bpm_used_names',...
        'f_s','selected_raw_for_bpm','fh','fv',...
        'xstd','ystd','Axa','Aya','Mxa','Mya','Axb','Ayb','Mxb','Myb','Ax0','Ay0','Mx0','My0','nusamp');
end

%%
% meas_folder='/data/MML/WorkDirectory/zeus/FBBA/measurements/';
% filecase=[meas_folder 'FBBA_skew_DC_Nov_11_2019-00-05-46.mat'];
% load(filecase);
% nbpm=size(BPM_centre,1);
% 
% for ibpm=1:nbpm
%     ibpm
%     jbpm=list_bpms(ibpm);
%     [BPM_centre_CIA(ibpm,:), BPM_error_CIA(ibpm,:)]=find_offsets(jbpm,Axa(:,:,ibpm),Aya(:,:,ibpm),Mxa(:,:,ibpm),Mya(:,:,ibpm),Axb(:,:,ibpm),Ayb(:,:,ibpm),Mxb(:,:,ibpm),Myb(:,:,ibpm));
% end
% subplot(2,1,1);
% plot(BPM_centre(:,1));hold all;
% plot(BPM_centre_CIA(:,1));
% subplot(2,1,2);
% plot(BPM_centre(:,2));hold all;
% plot(BPM_centre_CIA(:,2));
% 
% save(filecase,'-append','BPM_centre_CIA','BPM_error_CIA')

%% Return to FRM settings
fprintf('\nFRM mode preparation follows...\n\n');
preparation_frm;
