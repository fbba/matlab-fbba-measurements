%{
    Copyright (C) 2020  Zeus Martí Díaz

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
%}

function WVFStop_group(GroupId)
%% Function Name: WVFStop_group
% WVFStop_group(GroupId)
%
% Assumptions: sent stop command to a group of tango DS
% also valid for tango binding before 2015
%
% Inputs:
%  GroupId: tango grup ID 
%
% Outputs:
%   None

%
% Date: Jan 30, 2020
% ________________________________________

ver=version('-release');
if str2double(ver(1:4))>2015
    tango_group_command_inout_asynch(GroupId,'WVFStop',0,0,false);
else
    tango_group_command_inout_asynch(GroupId,'WVFStop',0,0,uint8(0));
end

end
