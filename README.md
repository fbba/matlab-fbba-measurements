# The Fast Beam Based Alignement Project

[![Python 2.7](https://img.shields.io/badge/Python-2.7.13-green.svg)](https://www.python.org/downloads/release/python-2713/)

[![Matlab 9.2.0.556344 (R2017a)](https://img.shields.io/badge/Matlab-R2017a-green.svg)]()

[![MML 2009](https://img.shields.io/badge/MML-2009-green.svg)]()

[![tango binding](https://img.shields.io/badge/tangobinding-3.1.0-green.svg)]()

## Introduction

In this repository you can find the compilation of all scripts related to the Fast Beam Based Alignement measurement.


For more information about the measurement method, please read the paper published on [Physical Review Accelerators and Beams](https://journals.aps.org/prab/abstract/10.1103/PhysRevAccelBeams.23.012802)

## Related projects


If you are interested in the matlab FBBA simulations on a cluster using SLURM see the [FBBA measurement](https://gitlab.com/fbba/matlab-fbba-measurements).

If you are interested in a python version of the scripts integrated in a GUI see the [FBBA python GUI](https://gitlab.com/fbba/python_fbba_gui).


## Licencing
All the code is GNU-GPLv3 licenced. Author and code mantainer calls for respecting the terms of this license. In case licence therms will be not respected, please contact author.

In case you will use this code, or parts of code contained in this repository, please cite.

## Contact
You can contact with project author at: zeus@cells.es.





